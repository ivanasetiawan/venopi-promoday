import React from 'react';
import {render} from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'

import './stylesheets/css/normalize.css';
import './stylesheets/css/style.css';

import Venue from './components/Venue';
import VenuePicker from './components/VenuePicker';
import NoMatch from './components/NoMatch';
import registerServiceWorker from './registerServiceWorker';
const repo = `/${window.location.pathname.split('/')[1]}`;

const Root = () =>  {
  return (
    <Router basename={repo}>
      <div>
        <nav className="mainnav">
          <h1 className="mainnav__title">
            <Link to="/">
              <img src="/venopi.svg" alt="Venopi" className="mainnav__venopi" />
            </Link>
          </h1>
          <ul className="h-clearlist mainnav__ul">
           <li><Link to="https://www.venopi.com/" className="btn">Check out Venopi</Link></li>
          </ul>
        </nav>

        <main>
          <Switch>
            <Route exact path="/" component={VenuePicker}/>
            <Route exact path="/venue/:venueId" component={Venue}/>
            <Route component={NoMatch}/>
          </Switch>
        </main>
      </div>
    </Router>
  )
}

render(<Root/>, document.querySelector('#root'));
registerServiceWorker();