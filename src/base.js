import Rebase from 're-base';
import firebase from 'firebase';

const app = firebase.initializeApp({
  apiKey: "AIzaSyAQ5i1KgjDFM5M2rdjLmy0VqctJ8iNyDd0",
  authDomain: "venopi-react.firebaseapp.com",
  databaseURL: "https://venopi-react.firebaseio.com"
});

const base = Rebase.createClass(app.database());

export default base;
