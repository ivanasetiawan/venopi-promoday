import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../stylesheets/css/promo.css';

class Promo extends Component {
  render() {
    const {details, index} = this.props;
    const isAvailable = details.status === 'available';
    const buttonText = isAvailable ? 'Add to order' : 'No longer available';

    return (
      <li className={"promo-list" + (isAvailable ? '' : ' unavailable')}>
        <img src={details.image} alt={details.title} className="promo-list__img" />
        <h2 className="promo-list__title">
          {details.title}
        </h2>
        <p className="promo-list__description">
          {details.description}
        </p>
        <p className="promo-list__price">
          &euro; {details.price}
        </p>
        <button onClick={() => this.props.addToOrder(index)} className="promo-list__btn btn" disabled={!isAvailable}>{buttonText}</button>
      </li>
    );
  }
}

Promo.propTypes = {
  details: PropTypes.object.isRequired,
  index: PropTypes.string.isRequired,
  addToOrder: PropTypes.func.isRequired
}

export default Promo;