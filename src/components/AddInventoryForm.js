import React, { Component } from 'react';
import PropTypes from 'prop-types';

class AddInventoryForm extends Component {
  createPromo(event) {
    event.preventDefault();
    const promo = {
      title: this.title.value,
      status: this.status.value,
      price: this.price.value,
      image: this.image.value,
      description: this.description.value
    }

    this.props.addPromo(promo);
    this.promoForm.reset();
  }

  render() {
    return (
      <div className="inventory-edit">
        <h2 className="inventory-edit__subtitle">Create a new promo</h2>
        <form onSubmit={(e) => this.createPromo(e)} ref={(input) => this.promoForm = input}>
          <div className="inventory-edit__multiple">
            <input ref={(input) => this.title = input} type="text" placeholder="Promo title"/>
            <input ref={(input) => this.price = input} type="number" placeholder="Promo price"/>
            <select ref={(input) => this.status = input}>
              <option value="available">Available</option>
              <option value="unavailable">Sold out</option>
            </select>
          </div>
          <input ref={(input) => this.image = input} type="text" placeholder="Promo image" className="inventory-edit__single-input" />
          <textarea ref={(input) => this.description = input} type="text" placeholder="Promo description" className="inventory-edit__single-input"></textarea>
          <button type="submit" className="btn inventory-edit__btn">Add promo</button>
        </form>
      </div>
    );
  }
}

AddInventoryForm.propTypes = {
  addPromo: PropTypes.func.isRequired
}

export default AddInventoryForm;