import React from 'react';

const NoMatch = () => {
  return (
    <p>Oopsie! No match!</p>
  );
}

export default NoMatch;