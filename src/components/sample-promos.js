const promos = {
  promo1: {
    title: '2 for 1',
    status: 'available',
    price: 500,
    image: 'https://images.unsplash.com/photo-1462446892934-2c17979efefd?dpr=1&auto=compress,format&fit=crop&w=376&h=251&q=80&cs=tinysrgb&crop=',
    description: 'In non ex eget turpis ultrices luctus. Morbi eleifend, est in posuere molestie, nunc nisl consequat elit, eu ultrices ipsum ante ac purus.'
  },
  promo2: {
    title: 'Free 2 hours rent',
    status: 'available',
    price: 800,
    image: 'https://images.unsplash.com/photo-1499358702852-b932042d7893?dpr=1&auto=compress,format&fit=crop&w=376&h=251&q=80&cs=tinysrgb&crop=',
    description: 'Quisque vel elit vestibulum, gravida eros quis, ornare quam. Maecenas sagittis ullamcorper est a porta.'
  },
  promo3: {
    title: 'First drink for free',
    status: 'unavailable',
    price: 70,
    image: 'https://images.unsplash.com/photo-1458639817867-2c9d4c5dcad4?dpr=1&auto=compress,format&fit=crop&w=376&h=251&q=80&cs=tinysrgb&crop=',
    description: 'Donec pulvinar nunc elit, a facilisis sem volutpat sit amet. Sed pellentesque ante erat, quis blandit velit ultricies non. Ut posuere pellentesque blandit.'
  },
  promo4: {
    title: '50% discount',
    status: 'unavailable',
    price: 80,
    image: 'https://images.unsplash.com/photo-1469371670807-013ccf25f16a?dpr=1&auto=compress,format&fit=crop&w=376&h=251&q=80&cs=tinysrgb&crop=',
    description: 'Donec pulvinar nunc elit, a facilisis sem volutpat sit amet. Sed pellentesque ante erat, quis blandit velit ultricies non. Ut posuere pellentesque blandit.'
  },
  promo5: {
    title: '2 for 1',
    status: 'available',
    price: 500,
    image: 'https://images.unsplash.com/photo-1462446892934-2c17979efefd?dpr=1&auto=compress,format&fit=crop&w=376&h=251&q=80&cs=tinysrgb&crop=',
    description: 'In non ex eget turpis ultrices luctus. Morbi eleifend, est in posuere molestie, nunc nisl consequat elit, eu ultrices ipsum ante ac purus.'
  },
  promo6: {
    title: 'Free 2 hours rent',
    status: 'available',
    price: 800,
    image: 'https://images.unsplash.com/photo-1499358702852-b932042d7893?dpr=1&auto=compress,format&fit=crop&w=376&h=251&q=80&cs=tinysrgb&crop=',
    description: 'Quisque vel elit vestibulum, gravida eros quis, ornare quam. Maecenas sagittis ullamcorper est a porta.'
  },
  promo7: {
    title: 'First drink for free',
    status: 'unavailable',
    price: 70,
    image: 'https://images.unsplash.com/photo-1458639817867-2c9d4c5dcad4?dpr=1&auto=compress,format&fit=crop&w=376&h=251&q=80&cs=tinysrgb&crop=',
    description: 'Donec pulvinar nunc elit, a facilisis sem volutpat sit amet. Sed pellentesque ante erat, quis blandit velit ultricies non. Ut posuere pellentesque blandit.'
  },
  promo8: {
    title: '50% discount',
    status: 'unavailable',
    price: 80,
    image: 'https://images.unsplash.com/photo-1469371670807-013ccf25f16a?dpr=1&auto=compress,format&fit=crop&w=376&h=251&q=80&cs=tinysrgb&crop=',
    description: 'Donec pulvinar nunc elit, a facilisis sem volutpat sit amet. Sed pellentesque ante erat, quis blandit velit ultricies non. Ut posuere pellentesque blandit.'
  }
}

export default promos;