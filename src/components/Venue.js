import React, { Component } from 'react';
import '../stylesheets/css/normalize.css';
import '../stylesheets/css/style.css';

import Header from './Header';
import Order from './Order';
import Inventory from './Inventory';
import Promo from './Promo';
import SamplePromos from './sample-promos';
import base from '../base';
// import VenueDetail from './VenueDetail';

class Venue extends Component {
  constructor() {
    super();

    // Load sample data
    this.loadSamples = this.loadSamples.bind(this);

    // Promo related
    this.addPromo = this.addPromo.bind(this);
    this.updatePromo = this.updatePromo.bind(this);
    this.removePromo = this.removePromo.bind(this);

    // Order related
    this.addToOrder = this.addToOrder.bind(this);
    this.removeFromOrder = this.removeFromOrder.bind(this);

    // init state
    this.state = {
      promos: {},
      order: {}
    };
  }

  componentWillMount() {
    // Runs right before <Venue> is rendered
    const path = `${this.props.match.path}`;
    this.ref = base.syncState(path, {
      context: this,
      state: 'promos'
    });

    // Check if there's already any localStorage orders
    const localStorageRef = localStorage.getItem(`orders`);
    if (localStorageRef) {
      // Update our orders' state
      this.setState({
        order: JSON.parse(localStorageRef)
      });
    }
  }

  componentWillUnmount() {
    base.removeBinding(this.ref);
  }

  componentWillUpdate(nextProps, nextState) {
    localStorage.setItem(`orders`, JSON.stringify(nextState.order));
  }

  addPromo(promo) {
    // Get the current state
    const promos = {...this.state.promos};
    // Add in the new promo
    const timestamp = Date.now();
    promos[`promo-${timestamp}`] = promo;
    // Set state
    this.setState({promos});
  }

  updatePromo(key, upatedPromo) {
    // Get the current state
    const promos = {...this.state.promos};
    promos[key] = upatedPromo;
    // Set state
    this.setState({promos});
  }

  removePromo(key) {
    // Get the current state
    const promos = {...this.state.promos};
    // Firebase way to remove (they don't do `delete`)
    promos[key] = null;
    // Set state
    this.setState({promos});
  }

  loadSamples() {
    this.setState({
      promos: SamplePromos
    });
  }

  addToOrder(key) {
    // Get the current order state
    const order = {...this.state.order};
    order[key] = order[key] + 1 || 1;
    // Set state
    this.setState({order});
  }

  removeFromOrder(key) {
    // Get the current order state
    const order = {...this.state.order};
    // Use `delete` because we are not restricted to Firebase
    delete order[key];
    // Set state
    this.setState({order});
  }

  render() {
    return (
      <section className="results">
        <article className="results__promos">
          <Header tagline="Your day to shine"/>
          <ul className="promos-ul h-clearlist">
            {
              Object.keys(this.state.promos)
              .map(key => <Promo key={key} index={key} details={this.state.promos[key]} addToOrder={this.addToOrder} />)
            }
          </ul>
        </article>

        <aside className="results__aside">
          <Order
            promos={this.state.promos}
            order={this.state.order}
            removeFromOrder={this.removeFromOrder}
           />

          <Inventory
            addPromo={this.addPromo}
            loadSamples={this.loadSamples}
            promos={this.state.promos}
            updatePromo={this.updatePromo}
            venueId={this.props.match.params.venueId}
            removePromo={this.removePromo}
            />
        </aside>
      </section>
    )
  }
}

export default Venue;