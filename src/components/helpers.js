export function rando(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

export function slugify(text) {
  return text.toString().toLowercase()
    .replace(/\s+/g, '-')
    .replace(/[^\w\-]+/g, '')
    .replace(/\-\-+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '');
}

export function getVenueName() {
  const adjs = ['adorable', 'beautiful', 'cozy', 'charming', 'fun', 'classic', 'unique'];
  const locations = ['venue', 'space', 'studio', 'atelier', 'garden', 'pool'];
  const cities = ['amsterdam', 'rotterdam', 'delft', 'haarlem', 'zandvoort', 'maastricht'];

  return `${rando(adjs)}-${rando(locations)}-in-${rando(cities)}`;
}