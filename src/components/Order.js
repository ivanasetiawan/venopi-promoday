import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import '../stylesheets/css/order.css';

class Order extends Component {
  constructor() {
    super();
    this.renderOrder = this.renderOrder.bind(this);
  }

  renderOrder(key) {
    const promo = this.props.promos[key];
    const count = this.props.order[key];
    const removeOrderButton = <a onClick={() => this.props.removeFromOrder(key)} className="order-item__remove">X</a>

    if (!promo || promo.status === 'unavailable') {
      return <li key={key}>Sorry, {promo ? promo.title : 'promo'} is no longer unavailable {removeOrderButton}</li>
    }

    return (
      <li key={key} className="order-item">
        <span className="order-item__title">
          <span className="order-item__count">{count}x</span>
          {promo.title}
          {removeOrderButton}
        </span>
        <span className="order-item__price">&euro; {count * promo.price}</span>
      </li>
    )
  }

  render() {
    const orderIds = Object.keys(this.props.order);
    const total = orderIds.reduce((prevTotal, key) => {
      const promo = this.props.promos[key];
      const count = this.props.order[key];
      const isAvailable = promo && promo.status === 'available';

      if (isAvailable) {
        return prevTotal + (count * promo.price || 0)
      }

      return prevTotal;
    }, 0);

    return (
      <article className="order">
        <h2 className="order__title">Your order</h2>

        <ReactCSSTransitionGroup
          className="order-ul h-clearlist"
          component="ul"
          transitionName="order"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={500}
        >
          {orderIds.map(this.renderOrder)}
          <li className="order__total">
            Total
            <strong className="order__total-amount">&euro; {total}</strong>
          </li>
        </ReactCSSTransitionGroup>

      </article>
    );
  }
}

Order.PropTypes = {
  promos: PropTypes.object.isRequired,
  order: PropTypes.object.isRequired,
  removeFromOrder: PropTypes.func.isRequired
}

export default Order;