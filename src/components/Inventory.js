import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AddInventoryForm from './AddInventoryForm';
import firebase from 'firebase';

import '../stylesheets/css/inventory.css';
import '../stylesheets/css/login.css';

class Inventory extends Component {
  constructor() {
    super();
    this.state = {
      isHidden: true,
      uid: null,
      owner: null
    }

    this.toggleHidden = this.toggleHidden.bind(this);
    this.toggleText = this.toggleText.bind(this);
    this.renderInventory = this.renderInventory.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.renderLogin = this.renderLogin.bind(this);
    this.authenticate = this.authenticate.bind(this);
    this.logout = this.logout.bind(this);
  }

  toggleHidden () {
    this.setState({
      isHidden: !this.state.isHidden
    })
  }

  visibleClass () {
    if (this.state.isHidden === true) {
      return 'hidden';
    }
    return 'visible';
  }

  toggleText () {
    if (this.state.isHidden === true) {
      return 'Your inventory';
    }
    return 'Close inventory';
  }

  handleChange(ev, key) {
    const promo = this.props.promos[key];
    // Get copy of the promo & update it with new data
    const updatedPromo = {
      ...promo,
      [ev.target.name]: ev.target.value
    };
    this.props.updatePromo(key, updatedPromo);
  }

  authenticate(AuthProvider) {
    const auth = firebase.auth();
    const venueId = this.props.venueId;
    const component = this;

    auth.signInWithPopup(AuthProvider).then(function(result) {
      // User signed in!
      const uid = result.user.uid;
      // Grab the venue info
      const venueRef = firebase.database().ref(venueId);

      // Query the firebase once for the venue data
      venueRef.once('value', (snapshot) => {
        const data = snapshot.val() || {};

        // Claim it as our own if there's no owner
        if (!data.owner) {
          venueRef.set({
            owner: uid
          });
        }

        component.setState({
          uid: uid,
          owner: data.owner || uid
        });

      });
    }).catch(function(error) {
      // An error occurred
      console.log('error', error);
    });
  }

  logout() {
    this.setState({
      uid: null
    });
  }

  renderLogin() {
    return (
      <section className="login">
        <h2 className="login__title">Login</h2>
        <p>Sign in to manage your venue's inventory</p>
        <a className="btn login__facebook" onClick={(e) => this.authenticate(new firebase.auth.FacebookAuthProvider())}>
          Facebook
        </a>
        <a className="btn login__twitter" onClick={(e) => this.authenticate(new firebase.auth.TwitterAuthProvider())}>
          Twitter
        </a>
        <a className="btn login__github" onClick={(e) => this.authenticate(new firebase.auth.GithubAuthProvider())}>
          Github
        </a>
      </section>
    )
  }

  renderInventory(key) {
    const promo = this.props.promos[key];
    return (
      <div className="inventory-edit" key={key}>
        <div className="inventory-edit__multiple">
          <input
            type="text"
            name="title"
            placeholder="Promo title"
            value={promo.title}
            onChange={(ev) => this.handleChange(ev, key)}/>

          <input
            type="number"
            name="price"
            value={promo.price}
            onChange={(ev) => this.handleChange(ev, key)}
            placeholder="0.00" />

          <select
            value={promo.status}
            name="status"
            onChange={(ev) => this.handleChange(ev, key)}
            >
            <option value="available">Available</option>
            <option value="unavailable">Sold out</option>
          </select>
        </div>

        <input
          className="inventory-edit__single-input"
          type="text"
          name="image"
          value={promo.image}
          onChange={(ev) => this.handleChange(ev, key)}
          placeholder="Image URL" />

        <textarea
          className="inventory-edit__single-input"
          type="text"
          name="description"
          value={promo.description}
          onChange={(ev) => this.handleChange(ev, key)}
          placeholder="Descriptiom">
        </textarea>

        <a
          className="inventory-edit__btn-rm btn"
          onClick={() => this.props.removePromo(key)}
        >
          Remove
        </a>
      </div>
    )
  }

  render() {
    const logout = <a className="btn btn--logout inventory__logout" onClick={this.logout}>Log out</a>;

    // Check if NOT logged in
    if (!this.state.uid) {
      return (
        <div>{this.renderLogin()}</div>
      )
    }

    // Check if the user is the OWNER of the current store
    if (this.state.uid !== this.state.owner) {
      return (
        <div className="inventory__owner-err">
          <p className="inventory__owner-err__p">Oopsie! You're not the owner of this Venue!</p>
          <a className="btn btn--logout" onClick={this.logout}>Log out</a>
        </div>
      )
    }

    return (
      <div className="inventory-box">
        <a className="inventory__toggle btn" onClick={this.toggleHidden.bind(this)}>
          {this.toggleText()}
        </a>

        {logout}

        <article className={'inventory ' + this.visibleClass()}>
          <div className="inventory-wrap">
            <h2 className="inventory__title">Your promo inventory</h2>

            <button className="inventory__btn-load btn" onClick={this.props.loadSamples}>
              Load sample promos
            </button>

            <AddInventoryForm addPromo={this.props.addPromo}/>

            {Object.keys(this.props.promos).map(this.renderInventory)}
          </div>
        </article>
      </div>
    );
  }
}

Inventory.propTypes = {
  promos: PropTypes.object.isRequired,
  updatePromo: PropTypes.func.isRequired,
  removePromo: PropTypes.func.isRequired,
  loadSamples: PropTypes.func.isRequired,
  addPromo: PropTypes.func.isRequired
}

export default Inventory;