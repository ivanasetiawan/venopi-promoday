import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {getVenueName} from './helpers';
import '../stylesheets/css/picker.css';

class VenuePicker extends Component {
  constructor() {
    super();
    this.goToVenue = this.goToVenue.bind(this);
  }

  goToVenue(e) {
    e.preventDefault();
    const venueId = this.storeInput.value;
    this.context.router.history.push(`/venue/${venueId}`);
  }

  render() {
    return (
      <div className="venue-picker-wrapper">
        <section className="venue-picker">
          <h1 className="venue-picker__title">
            <span className="venue-picker__tag">Welcome to</span>
            <br />
            Venopi Promoday
          </h1>

          <form
            className="venue-picker__form"
            onSubmit={this.goToVenue}
            >

            <input
              className="venue-picker__input"
              type="text"
              required
              placeholder="Venue unique name"
              ref={(input) => {this.storeInput = input}}
              defaultValue={getVenueName()}
              />
            <button
              type="submit"
              className="btn venue-picker__btn"
            >
              Visit your venue
            </button>
          </form>

          <div className="venue-picker__about">
            <h2 className="venue-picker__subtitle">
              About this project
            </h2>
            <p className="venue-picker__p">
              <strong>"Venopi Promoday"</strong> is a fun React project created by <a href="http://ivanasetiawan.com/" target="_blank" rel="noopener noreferrer">Ivana</a> for one of Venopi's potential features in the future. The main idea of the project is to explore the React framework.
              <br /><br />
              How does it work? Just hit <strong>`visit your venue`</strong> button and we will take you to your promoday dashboard! So easy!
            </p>
          </div>
        </section>
        <img src="/party.jpg" alt="Venopi Promoday" className="venue-picker__img" />
      </div>
    );
  }
}

VenuePicker.contextTypes = {
  router: PropTypes.object
}

export default VenuePicker;