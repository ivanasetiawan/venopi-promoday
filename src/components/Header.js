import React from 'react';
import PropTypes from 'prop-types';
import '../stylesheets/css/mainheader.css';

const Header = (props) => {
  return (
    <header className="mainheader">
      <h1 className="mainheader__title">Promoday</h1>
      <h3 className="mainheader__tagline">
        <span className="mainheader__tagline__text">{props.tagline}</span>
      </h3>
    </header>
  );
}

Header.propTypes = {
  tagline: PropTypes.string.isRequired
}

export default Header;