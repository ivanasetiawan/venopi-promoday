import React from 'react';

const VenueDetail = ({ match }) => (
  <div>
    <h3>{match.params.venueId}</h3>
  </div>
)

export default VenueDetail;