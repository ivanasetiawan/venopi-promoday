# ABOUT THIS PROJECT #

"Venopi Promoday" is a fun React project created by Ivana for one of Venopi's potential features in the future. The main idea of the project is to explore the React framework.

How does it work? Just hit `visit your venue` button and we will take you to your promoday dashboard! So easy!

You need to login using Github, Facebook, or Twitter to be able to access your fun "Venue".

### What is this repository for? ###
* Venopi promo day prototype
* Try React(v16.0.0)

### How do I get set up? ###
* Clone the project
* run `yarn install`
* run `yarn start`

### Screenshots ###
#### Homepage ####
![Homepage](docs/vpromoday-1.jpg)

#### Venue detail ####
![Venue detail](docs/vpromoday-2.jpg)

#### Logged in ####
![Logged in](docs/vpromoday-3.jpg)

#### Logged in but not the owner ####
![Logged in, not owner](docs/vpromoday-3-2.jpg)

#### Update inventory(only for logged in user) ####
![Update inventory](docs/vpromoday-4.jpg)

### Todo lists ###
* Tests
